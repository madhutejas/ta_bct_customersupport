package api.endpoints;

public enum WebServiceEndPoints {

	FormatUPU("http://webservices-dca-ac2.netpost/ws/MailingAddressProofingCSREST_v1/address/formatUPUAddresses");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
