package api.formataddress;

import api.endpoints.WebServiceEndPoints;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class FormatUPUPostCall {
	 @Step("Post FormatUPUAddress")
	    public void withDetails(String trade) {
	        SerenityRest.given()
	                .contentType("application/json")
	                .header("Content-Type", "application/json")
	                .body(trade)
	                .when()
	                .post(WebServiceEndPoints.FormatUPU.getUrl());
}
}