package api.formataddress;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import api.endpoints.*;
import io.restassured.RestAssured;

public class FormatUPURequest {


    @Step("Send FormatUPU POST request")
    public static void withDetails(String trade) {
        RestAssured.given()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(trade)
                .when()
                .post(WebServiceEndPoints.FormatUPU.getUrl());
    }
}
