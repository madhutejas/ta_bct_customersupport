package ddm.gui.runner.core;
import io.cucumber.core.snippets.SnippetType;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.aspectj.lang.annotation.After;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		plugin = {"pretty","json:target/cucumber-reports/Cucumber.json"	},
         features = "src/test/resources/features/Preparation10/",
        //features = "src/test/resources/features/Extra/",
        glue = {"preparation.app.stepdefination"},
     // tags = {"@TEST_MON-1421"},
        strict = true,
        snippets= io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE)

public class DDMTestSuite {

    @AfterClass
    public static void teardown() {
        System.out.println("Ran the after");
    }
}
//franking.app.stepdefination preparation.app.stepdefination

//1 @TEST_OXF-1088
// 2 @TEST_OXF-1094
//3 @TEST_OXF-1107
//4 @TEST_OXF-1108
//5 @TEST_OXF-1109
//6 @TEST_OXF-1110  barcode required
//7 @TEST_OXF-1111
//8 @TEST_OXF-1114
//9 @TEST_OXF-1115
//10 @TEST_OXF-1116 barcode required
//11 @TEST_OXF-1117
//12 @TEST_OXF-1118
//13 @TEST_OXF-1136
//14 @TEST_OXF-1137
//15 @TEST_OXF-1147