package jira.client.integration.core;

import java.util.Arrays;
import java.util.List;

import jira.client.integration.*;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class Jiramain {

	private static String Jirausername,JiraPassword,JiraURL,Issuekey;
	
	public static void main(String[] args) throws JiraException {
		// TODO Auto-generated method stub
		
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
		
		Jirausername=variables.getProperty("Username");
		JiraPassword=variables.getProperty("Password");
		JiraURL=variables.getProperty("URL");
		Issuekey=variables.getProperty("IssueNo");
		
		
		  BasicCredentials creds = new BasicCredentials(Jirausername,JiraPassword);
	      JiraClient jira = new JiraClient(JiraURL, creds);
	        try {
	        	
	        	String [] items =Issuekey.split("\\s*,\\s*");
	        	
	        	//System.out.println(items[1]);
	        	//System.out.println(items[0]);
	        	
	        	  for (int i = 0; i < items.length; i++){
	            /* Retrieve issue TEST-123 from JIRA. We'll get an exception if this fails. */
	            Issue issue = jira.getIssue(items[i]);

	            /* Print the issue key. */
	            System.out.println(issue);

	            /* You can also do it like this: */
	            System.out.println(issue.getKey());

	            /* Vote for the issue. */
	            issue.vote();
	            
	           issue.addComment("Test Automation results uploaded: Please refer the link to detail report");
	        	  }
	        }

	           catch (JiraException ex) {
	            System.err.println(ex.getMessage());

	            if (ex.getCause() != null)
	                System.err.println(ex.getCause().getMessage());
	        }
	}

}
