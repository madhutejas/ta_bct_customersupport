package preparation.app.stepdefination;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.cucumber.java.en.Then;
import preparation.test.pages.BCT_CreateNewReq_NotReceived_Page;

public class BCT_CreateNewReq_NotReceived_Steps extends OSPLoginBase_Pages {
	
	
@Then ("User create New Request type NotReceived")

public void UsercreateNewRequesttypeNotReceived() throws InterruptedException {
	
	BCT_CreateNewReq_NotReceived_Page obj1 = new BCT_CreateNewReq_NotReceived_Page(driver);
	
	obj1.searchReqZip();
	obj1.createNewReq();
	obj1.getBarcode();
		
	}
}	

