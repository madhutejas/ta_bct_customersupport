package preparation.app.stepdefination;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import preparation.test.pages.BCT_SearchNewReq_Pages;

public class BCT_SearchNewReq_Steps extends OSPLoginBase_Pages {


@And ("User search New Parcel Req with zipcode")
public void userSearchNewParcelReqWithZipcode() throws InterruptedException {
    // Write code here that turns the phrase above into concrete actions
	
	//driver.findElement(By.xpath("//input[@name=\"zipcode\"]")).sendKeys("1000");
	//driver.findElement(By.xpath("//button[@name=\"submit\"]")).click();
		
	BCT_SearchNewReq_Pages bct_search = new BCT_SearchNewReq_Pages(driver);
	
	bct_search.Zipcode().sendKeys("1000");
	bct_search.SubmitBtn().click();
	
	Thread.sleep(10000L);
	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	
	String BARCODE = driver.findElement(By.xpath("//div/table/tbody[1]/tr[1]/td[1]/a[1]")).getText();
	driver.findElement(By.xpath("//div[@class='custom-header']/div[2]/div[2]/div[2]/a")).click();
	
	driver.findElement(By.xpath("//div[@class='custom-header']/div[2]/div[2]/div[3]/a")).click();
	Thread.sleep(3000L);
	driver.findElement(By.xpath("//input[@name='barcode']")).sendKeys(BARCODE);
	//driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	//driver.findElement(By.xpath("//button[contains(text(),'Zoek')]")).click();
	driver.findElement(By.xpath("//button[contains(text(),'Search')]")).click();
	Thread.sleep(5000L);
	driver.findElement(By.xpath("//button[contains(text(),'Search')]")).click();
	Thread.sleep(3000L);
	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	Thread.sleep(3000L);
	driver.findElement(By.xpath("//td[contains(text(),'Details')]")).click();
	Thread.sleep(3000L);
	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	System.out.println("END OF TEST");
}

@Then ("User gets the search result details")

public void Usergetsthesearchresultdetails() throws InterruptedException {
	//driver.quit();
	System.out.println("Test Run successful");	
}

}