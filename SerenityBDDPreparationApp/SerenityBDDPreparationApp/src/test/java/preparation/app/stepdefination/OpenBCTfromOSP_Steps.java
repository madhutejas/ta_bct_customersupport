package preparation.app.stepdefination;

import org.openqa.selenium.By;

import io.cucumber.java.en.Then;
import preparation.test.pages.Osp_Home_Page;

public class OpenBCTfromOSP_Steps extends OSPLoginBase_Pages{
	
	
	@Then("I click on BCT link on HomePage")
	public void iClickOnBCTLinkOnHomePage() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		//driver = InitializeDriver();
		
		System.out.println("Coding in progress");
		Osp_Home_Page obj = new Osp_Home_Page(driver);
		obj.getBCTlink().click();
		obj.gotoChildwindow();
		obj.getCookiepopup().click();
		obj.getNewReq().click();
		
		// Switch to new window opened
			/*	for(String winHandle : driver.getWindowHandles()){
				    driver.switchTo().window(winHandle);
				}
				
				Thread.sleep(3000L);
				driver.findElement(By.xpath("//*[@id=\'onetrust-accept-btn-handler\']")).click();
				driver.findElement(By.xpath("//a[contains(text(),'Nieuwe aanvraag pakje')]")).click(); */
		obj.selectEngPageLang();
		//driver.quit();

	}

}
