package preparation.app.stepdefination;

import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import preparation.test.pages.OspLandingPage;


@RunWith(Cucumber.class)
public class OspLogin_Steps extends OSPLoginBase_Pages {
	
	
	@Given("I open chrome browser")
	public void iOpenChromeBrowser() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("This is step1");
		driver = InitializeDriver();		
		// we can access method of other class by inheritance or
		// by creating obj of other class		
	}

	@When("I navigate to osp-portal login page")
	public void iNavigateToOspPortalLoginPage() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.get("https://osp-2.acbpost.be/portal/goHome");
		OspLandingPage ob = new OspLandingPage(driver);
		Thread.sleep(3000L);
		ob.getcookieBanner().click();
		System.out.println("This is step2");
	}

	@When("I provide username and password")
	public void iProvideUsernameAndPassword() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("This is step3");
		OspLandingPage ob = new OspLandingPage(driver);
		ob.getusername().sendKeys("119790");
		ob.getpassword().sendKeys("Test1234");
	}

	@When("I click on login button")
	public void iClickOnLoginButton() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("This is step4");
		OspLandingPage ob = new OspLandingPage(driver);
		ob.getLogin().click();
	}

	@Then("username should be on home page")
	public void usernameShouldBeOnHomePage() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000L);
		System.out.println("This is step5");
		//driver.quit();
	}

	
}
