package preparation.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BCT_CreateNewReq_NotReceived_Page {
	
	public WebDriver driver;
	
	public BCT_CreateNewReq_NotReceived_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver= driver;
		//PageFactory.initElements(driver, this); // Testcase object which we are initializing to local driver
	}
	
	public void searchReqZip() throws InterruptedException
	{
		driver.findElement(By.xpath("//input[@name=\"zipcode\"]")).sendKeys("1000");
		driver.findElement(By.xpath("//button[@name=\"submit\"]")).click();
		Thread.sleep(5000L);
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
		Thread.sleep(10000L);
		driver.findElement(By.xpath("//span[@title=\"Not received\"]")).click();
	}
	
	public void createNewReq() {
		
		//driver.findElement(By.xpath("//span[@title=\"Not received\"]")).click();
		driver.findElement(By.xpath("//textarea[@id=\"free-text\"]")).sendKeys("Auto");
		driver.findElement(By.xpath("//input[@name=\"first-name\"]")).sendKeys("User");
		driver.findElement(By.xpath("//input[@name=\"last-name\"]")).sendKeys("AutoTest"); 
		driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
		System.out.println(driver.getTitle());
	}
	
	public void getBarcode() throws InterruptedException {
	
	List<WebElement> Heading = driver.findElements(By.xpath("//p[@class=\"demand-detail-title\"]"));
		
		for(int i = 0; i< Heading.size();i++)
			
		{
			Thread.sleep(10000L);
			String name = Heading.get(i).getText();
			
			if (name.contains("Barcode"))
			{
				Thread.sleep(10000L);
				System.out.println(driver.findElements(By.xpath("//p[@class=\"demand-text\"]")).get(i).getText());
				System.out.println(driver.findElements(By.xpath("//p[@class=\"demand-text\"]")).get(i+1).getText());
				break;
			}
			
		}

	}

}
