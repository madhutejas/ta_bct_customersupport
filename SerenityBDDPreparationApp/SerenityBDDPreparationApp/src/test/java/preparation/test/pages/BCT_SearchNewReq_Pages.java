package preparation.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BCT_SearchNewReq_Pages {
	
	public WebDriver driver;
	
	public BCT_SearchNewReq_Pages(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver= driver;
		PageFactory.initElements(driver, this); // Testcase object which we are initializing to local driver
	}
	
	@FindBy(xpath="//input[@name=\"zipcode\"]")
	WebElement Zip;
	
	@FindBy(xpath="//button[@name=\"submit\"]")
	WebElement submit;

	public WebElement Zipcode()
	{
		return Zip;
	}
	
	public WebElement SubmitBtn()
	{
		return submit;
	}

}
