package preparation.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OspLandingPage {
	
	
	public WebDriver driver;
	
	public OspLandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	By cookieBanner = By.cssSelector("button[id=onetrust-accept-btn-handler]");
	By username = By.id("inputUsername");
	By password = By.id("inputPassword");
	By signin=By.cssSelector("button[class= 'form-control cre-red-btn']");
	

	public WebElement getcookieBanner()
	{
		return driver.findElement(cookieBanner);
	}
	
	public WebElement getusername()
	{
		return driver.findElement(username);
	}
	
	public WebElement getpassword()
	{
		return driver.findElement(password);
	}
	public WebElement getLogin()
	{
		return driver.findElement(signin);
	}
	
	

}
