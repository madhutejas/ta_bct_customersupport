package preparation.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Osp_Home_Page {
	
	public WebDriver driver;
	
	public Osp_Home_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}

	By LinkBCTonHome = By.linkText("Customer Support Tool");
	By CookiePopup = By.xpath("//*[@id=\'onetrust-accept-btn-handler\']");
	By NewReq = By.xpath("//a[contains(text(),'Nieuwe aanvraag pakje')]");
	
	public WebElement getBCTlink()
	{
		return driver.findElement(LinkBCTonHome);
	}
	public WebElement getCookiepopup()
	{
		return driver.findElement(CookiePopup);
	}
	public WebElement getNewReq()
	{
		return driver.findElement(NewReq);
	}
	// Switch control to child windowOpened
	public void gotoChildwindow() throws InterruptedException
	{
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		Thread.sleep(3000L);
	}
	//Select static Dropdown List Item by index
	public void selectEngPageLang()
	{
		WebElement StaticDropList = driver.findElement(By.xpath("//header/div[1]/div[1]/div[3]/select[1]"));
		Select dropdown = new Select(StaticDropList);
		dropdown.selectByIndex(0);
		System.out.println(driver.getTitle());
	}
	

}
