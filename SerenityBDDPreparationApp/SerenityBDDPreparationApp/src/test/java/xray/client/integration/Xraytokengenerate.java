package xray.client.integration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;


public class Xraytokengenerate {

	 static EnvironmentVariables Prop = SystemEnvironmentVariables.createEnvironmentVariables();
	 
	 private static String AuthEndpoint;
	 private static String Client_id;
	 private static String Client_secret;
	 
	 
	 protected static String Xray() {
		 
		 AuthEndpoint=Prop.getProperty("Auth_Endpoint");
		 Client_id=Prop.getProperty("Client_id");
		 Client_secret=Prop.getProperty("Client_SecretKey");
		 
		    URL url = null;
		    InputStream stream = null;
		    HttpURLConnection urlConnection = null;
		    try {
		        url = new URL(AuthEndpoint);
		        urlConnection = (HttpURLConnection) url.openConnection();
		        urlConnection.setRequestMethod("POST");
		        urlConnection.setDoOutput(true);

		        String data = URLEncoder.encode("client_id", "UTF-8")
		                + "=" + URLEncoder.encode(Client_id, "UTF-8");

		        data += "&" + URLEncoder.encode("client_secret", "UTF-8") + "="
		                + URLEncoder.encode(Client_secret, "UTF-8");

		        urlConnection.connect();

		        OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		        wr.write(data);
		        wr.flush();

		        stream = urlConnection.getInputStream();
		        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
		        String result = reader.readLine();
		        System.out.println("Token Generated:"+ result);
		        return result;

		    } catch (IOException e) {
		        e.printStackTrace();
		    } finally {
		        if (urlConnection != null) {
		            urlConnection.disconnect();
		        }
		    }

		    try {
		        Thread.sleep(2000);
		    } catch (InterruptedException e) {
		        e.printStackTrace();
		     
		    }
		    return null;
		}
	 
   public static void main(String[] args) throws Exception {
		   
	   Xray();
		   
	   }
}
