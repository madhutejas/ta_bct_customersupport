#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
 Feature: Create New Parcel Request from BCT portal
 
  @double
  Scenario: Successful creation of New Request from BCT
    Given I open chrome browser
    When I navigate to osp-portal login page
    And I provide username and password
    And I click on login button
    Then username should be on home page
    And I click on BCT link on HomePage
    Then User create New Request type NotReceived
