#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps

 Feature: Search Parcel Request from BCT portal
 
  @single
  Scenario: Successful search results from BCT
    Given I open chrome browser
    When I navigate to osp-portal login page
    And I provide username and password
    And I click on login button
    Then username should be on home page
    And I click on BCT link on HomePage
    And User search New Parcel Req with zipcode
    Then User gets the search result details